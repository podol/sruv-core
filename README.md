# SRU 5
## System Rejestracji Użytkowników V
Authors: SKOS PG - admin@ds.pg.gda.pl - http://skos.ds.pg.gda.pl 

# Establishing environment by hand
## Running it
```console
$ sudo docker login registry.gitlab.com
[...]

$ sudo docker pull registry.gitlab.com/podol/docker-sruv-core
Using default tag: latest
Trying to pull repository registry.gitlab.com/podol/docker-sruv-core ... 
latest: Pulling from registry.gitlab.com/podol/docker-sruv-core
3690ec4760f9: Pull complete 
86df0df98d6a: Pull complete 
007744be7f18: Pull complete 
Digest: sha256:79e767c4430f88b0cef27986a5f8b38ea33e2240e9a8c4c7d6c70c9f4379e97c
Status: Downloaded newer image for registry.gitlab.com/podol/docker-sruv-core:latest
$ sudo docker pull registry.gitlab.com/podol/docker-sruv-postgres
Using default tag: latest
Trying to pull repository registry.gitlab.com/podol/docker-sruv-postgres ... 
latest: Pulling from registry.gitlab.com/podol/docker-sruv-postgres
3690ec4760f9: Already exists 
7d403be3833b: Pull complete 
Digest: sha256:3ef3967398aa81e61ed995b4ab6f502eb455ef5d45624e1aa8e87e275bdd84d7
Status: Downloaded newer image for registry.gitlab.com/podol/docker-sruv-postgres:latest

$ sudo mkdir -p /var/lib/postgresql/data
$ sudo chown -R 70:70 /var/lib/postgresql

$ sudo docker run -ti -v /var/lib/postgresql/data:/var/lib/postgresql/data:Z -u postgres registry.gitlab.com/podol/docker-sruv-postgres initdb
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locales
  COLLATE:  C
  CTYPE:    C.UTF-8
  MESSAGES: C
  MONETARY: C
  NUMERIC:  C
  TIME:     C
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/data ... ok
creating subdirectories ... ok
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting dynamic shared memory implementation ... posix
creating configuration files ... ok
creating template1 database in /var/lib/postgresql/data/base/1 ... ok
initializing pg_authid ... ok
initializing dependencies ... ok
creating system views ... ok
loading system objects' descriptions ... ok
creating collations ... sh: locale: not found
ok
No usable system locales were found.
Use the option "--debug" to see details.
creating conversions ... ok
creating dictionaries ... ok
setting privileges on built-in objects ... ok
creating information schema ... ok
loading PL/pgSQL server-side language ... ok
vacuuming database template1 ... ok
copying template1 to template0 ... ok
copying template1 to postgres ... ok
syncing data to disk ... ok

WARNING: enabling "trust" authentication for local connections
You can change this by editing pg_hba.conf or using the option -A, or
--auth-local and --auth-host, the next time you run initdb.

Success.

$ sudo docker run -td -v /var/lib/postgresql/data:/var/lib/postgresql/data:Z -u postgres registry.gitlab.com/podol/docker-sruv-postgres postgres
3a8e9db6cc1c191123a112fad563f417c066af75fea151139b833483c137919f

$ sudo docker run -td -p 8080:8080 -u sruser registry.gitlab.com/podol/docker-sruv-core /usr/bin/java -jar /var/lib/sruv/sruv.jar
4f647670dda47509c090bbdc8f1bc136977985729c9a492fb0bd6880d7ddf057
```

## Checking it
```console
$ sudo docker ps
CONTAINER ID        IMAGE                                            COMMAND                  CREATED             STATUS              PORTS                    NAMES
4f647670dda4        registry.gitlab.com/podol/docker-sruv-core       "/usr/bin/java -jar /"   16 seconds ago      Up 14 seconds       0.0.0.0:8080->8080/tcp   stupefied_heyrovsky
3a8e9db6cc1c        registry.gitlab.com/podol/docker-sruv-postgres   "postgres"               27 seconds ago      Up 24 seconds       5432/tcp                 goofy_feynman

$ sudo ls -lah /var/lib/postgresql/data
razem 60K
drwx------. 19 70 70 4,0K 11-29 21:20 .
drwxr-xr-x.  3 70 70   17 11-29 21:19 ..
drwx------.  5 70 70   38 11-29 21:20 base
drwx------.  2 70 70 4,0K 11-29 21:20 global
drwx------.  2 70 70   17 11-29 21:20 pg_clog
drwx------.  2 70 70    6 11-29 21:20 pg_commit_ts
drwx------.  2 70 70    6 11-29 21:20 pg_dynshmem
-rw-------.  1 70 70 4,4K 11-29 21:20 pg_hba.conf
-rw-------.  1 70 70 1,6K 11-29 21:20 pg_ident.conf
drwx------.  4 70 70   37 11-29 21:20 pg_logical
drwx------.  4 70 70   34 11-29 21:20 pg_multixact
drwx------.  2 70 70   17 11-29 21:20 pg_notify
drwx------.  2 70 70    6 11-29 21:20 pg_replslot
drwx------.  2 70 70    6 11-29 21:20 pg_serial
drwx------.  2 70 70    6 11-29 21:20 pg_snapshots
drwx------.  2 70 70    6 11-29 21:20 pg_stat
drwx------.  2 70 70   24 11-29 21:21 pg_stat_tmp
drwx------.  2 70 70   17 11-29 21:20 pg_subtrans
drwx------.  2 70 70    6 11-29 21:20 pg_tblspc
drwx------.  2 70 70    6 11-29 21:20 pg_twophase
-rw-------.  1 70 70    4 11-29 21:20 PG_VERSION
drwx------.  3 70 70   58 11-29 21:20 pg_xlog
-rw-------.  1 70 70   88 11-29 21:20 postgresql.auto.conf
-rw-------.  1 70 70  21K 11-29 21:20 postgresql.conf
-rw-------.  1 70 70   18 11-29 21:20 postmaster.opts
-rw-------.  1 70 70   78 11-29 21:20 postmaster.pid

$ curl -u admin 127.0.0.1:8080/health
Enter host password for user 'admin':
{"status":"UP","diskSpace":{"status":"UP","total":10725883904,"free":10510778368,"threshold":10485760},"db":{"status":"UP","database":"H2","hello":1}}
```

# Establishing environment with vagrant
Vagrant spins up the docker instances to bring up envrionment. There's no virtualization in between host and dockers (like vbox, kvm, etc.)
## Vagrantfile contnet
```console
VAGRANTFILE_API_VERSION = "2"
ENV['VAGRANT_NO_PARALLEL'] = 'yes'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.define "initdb" do |initdb|
    initdb.vm.provider "docker" do |d|
      d.image = "registry.gitlab.com/podol/docker-sruv-postgres"
      d.create_args = ["-u", "postgres"]
      d.volumes = ["/var/lib/postgresql/data:/var/lib/postgresql/data:Z"]
      d.cmd = ["initdb"]
      d.remains_running = false
      d.has_ssh = false
    end
  end

  config.vm.define "rundb" do |rundb|
    rundb.vm.provider "docker" do |d|
      d.image = "registry.gitlab.com/podol/docker-sruv-postgres"
      d.create_args = ["-u", "postgres"]
      d.volumes = ["/var/lib/postgresql/data:/var/lib/postgresql/data:Z"]
      d.cmd = ["postgres"]
      d.has_ssh = false
    end
  end

  config.vm.define "sruv" do |sruv|
    sruv.vm.provider "docker" do |d|
      d.image = "registry.gitlab.com/podol/docker-sruv-core"
      d.expose = ["8080"]
      d.create_args = ["-u", "sruser"]
      d.cmd = ["/usr/bin/java", "-jar", "/var/lib/sruv/sruv.jar"]
      d.has_ssh = false
    end
  end

end
```
## Usage
Here's the hard way - assuming SElinux. If you brave enough to... disable or not have one, you can skip "chcon" part.
1. Install docker and vagrant.
2. Run:

```console
mkdir -p /var/lib/postgresql/data
chown -R 70:70 /var/lib/postgresql
chcon -R -u system_u -t docker_var_lib_t /var/lib/postgresql
chcon -t svirt_sandbox_file_t /var/lib/postgresql/data
vagrant up
```
# Establishing environment with terraform
# Establishing environment with other tools
